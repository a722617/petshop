export default {
  isRounded: [true, false] as const,
  default: {
    isRounded: false,
  },
}