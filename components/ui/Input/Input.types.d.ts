import config from './Input.config'

export type InputRounded = (typeof config.isRounded)[number]