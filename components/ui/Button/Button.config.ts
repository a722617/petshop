export default {
  variant: ['normal', 'outline'] as const,
  isRounded: [true, false] as const,
  default: {
    variant: 'normal',
    isRounded: true,
  },
}