import config from './Button.config'

export type ButtonVariant = (typeof config.variant)[number]
export type ButtonRounded = (typeof config.isRounded)[number]